﻿using System;
using System.Text.RegularExpressions;
using System.Collections.Generic; 
using System.Linq;

namespace EmailFinder
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Input your text to start finding emails!");

            string text = Console.ReadLine();
            while (string.IsNullOrWhiteSpace(text))
            {
                Console.WriteLine("Input your text to start finding emails");
                text = Console.ReadLine();
            }

            MatchCollection emailMatch = FindEmail(text);
            int count = emailMatch.Count;

            if (count > 0)
            {
                Console.WriteLine($"A total of {count}, were found.");
                foreach(var email in emailMatch){
                    Console.WriteLine(email);
                }
            }
            else
            {
                Console.WriteLine($"No emails were found.");
            }

            Console.ReadKey();
            
        }

        static MatchCollection FindEmail(string text)
        {
            // define regex with email pattern
            Regex regex = new Regex(@"[a-zA-Z0-9._%+-]+@[a-zA-Z]+(\.[a-zA-Z0-9]+)+", RegexOptions.IgnoreCase);

            MatchCollection emailMatch = regex.Matches(text);
            return emailMatch;
        }
        
    }
}
